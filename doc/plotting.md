# Plotting

One of the major components of clrf is a set of tools to make plotting
in common lisp easier and more intuitive. clrf achieves this by
providing utilities for producing common plot types using eazy-gnuplot
to interface with gnuplot, which does the actual plotting.

clrf's plotting utilities are best introduced by example, so we will
proceed as such. For more detailed information about eazy-gnuplot and
gnuplot, please refer to the documentation of those projects.

## A simple line plot

The following code will produce a simple line plot of a sine and
cosine wave.

```common_lisp
(let* ((x (range 0 (* pi 1/100) (* 2 pi)))
       (y1 (loop for x-element across x collect (sin x-element)))
       (y2 (loop for x-element across x collect (cos x-element))))
  (with-plots (*standard-output*)
	      (gp-standard-setup
	       :eps *1col-graphic-size*
	       :xlabel "X"
	       :ylabel "Y"
	       :xrange :|[0:2*pi]|
	       :yrange :|[-1.5:1.5]|
	       :xtics :|(0, '{/Symbol p}/2' pi/2, '{/Symbol p}' pi, '3{/Symbol p}/2' 3*pi/2, '2{/Symbol p}' 2*pi)|
	       :ytics "1"
	       :tics '(:scale "1.25")
	       :output #p"sine-and-cosine.eps")
	      (gp-set-bw-lines)
	      (plot (gp<-xy x y1) :title "sin(x)" :with '(:lines))
	      (plot (gp<-xy x y2) :title "cos(x)" :with '(:lines))))
```

As described in the eazy-gnuplot documentation, the commands for each
output file are wrapped in a `with-plots` form. Each `with-plots` form
requires (at least) one `gp-setup` form to set plot attributes. To
reduce the tedium of writing out common terminal settings, clrf
provides the `gp-standard-setup` macro to setup a given terminal with
decent settings. `gp-standard-setup` requires a keyword for the
desired output terminal and an output size (or `nil`, if the user
doesn't want to explicitly set the graphic size), which are used to
generate the terminal settings, then all remaining arguments are
passed to `gp-setup` after the terminal settings. clrf defines the
dynamic parameters `*1col-graphic-size*` and `*2col-graphic-size*`
which will produce eps or epslatex graphics that are one or two
columns wide respectively, in ieee format. Most of the remaining
arguments in `gp-standard-setup` are fairly self-explanatory, although
it should be noted that some attributes somewhat abuse keywords. For
details about this, please refer to the eazy-gnuplot documentation.

After the `gp-standard-setup` form, `gp-set-bw-lines` is called to set
pretty black and white line styles. For color lines, clrf provides
`gp-set-color-lines` to set pretty color line styles.

Following the setup for the plot, the lines are finally plotted. The
first argument to a `plot` form is a function that prints the data to
be plotted. The macro `gp<-xy` takes n vectors, and produces a
function that will plot them against each other, much as matlab's
`plot()` command handles data vectors. `:title` sets the legend's
label for the line, and `:with '(:lines)` tells gnuplot to plot the
data with connecting lines. This code will produce [this
plot](images/sine-and-cosine.eps).
