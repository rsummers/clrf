(defmethod staple:output-directory ((system (eql (asdf:find-system :clrf))))
  (asdf:system-relative-pathname system "public/"))

(defmethod staple:packages ((system (eql (asdf:find-system :clrf))))
  (list (find-package :clrf)))

(defmethod staple:documents ((system (eql (asdf:find-system :clrf))))
  (list (asdf:system-relative-pathname system #p"README.md")))
