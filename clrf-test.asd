#|
  This file is a part of clrf project.
|#

(defsystem "clrf-test"
  :defsystem-depends-on ("prove-asdf")
  :author ""
  :license ""
  :depends-on ("clrf"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "clrf"))))
  :description "Test system for clrf"

  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
