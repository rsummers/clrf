clrf is an Open Source, MIT-licensed package for RF/Microwave
engineering using Common Lisp. clrf is very much in its infant stages,
and should be treated as such. However, some interesting features have
already been implemented, and any useage/feedback/contributions are
much appreciated. Utilities for generating plots and functions for
microstrip calculations have been implemented, with further
improvements planned, along with many other exciting features.

## Documentation

The README contains details on the basic usage of clrf. Documentation
strings are heavily used for documenting the code. Documentation
generated using staple can be found in the /public directory of clrf,
and at [rsummers.gitlab.io/clrf](https://rsummers.gitlab.io/clrf).

