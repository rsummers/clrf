(in-package :clrf)

(defparameter *1col-graphic-size* '|3.5,2.45|
  "Standard size for graphics intended to span 1 column in IEEE format.")
(defparameter *2col-graphic-size* '|7.16,5.01|
  "Standard size for graphics intended to span 2 columns in IEEE format.")
(defparameter *wxt-graphic-size* '|525,368|
  "A decent default size for the wxt terminal.")

(defun gp-terminal-list (terminal size)
  "
  Returns a list with decent default settings for the gnuplot terminal
  `terminal` with size `size`.

  `terminal` can be :wxt, :png, :svg, :eps, or :epslatex, and `size`
  can be either a size specification or nil.
  "
  (cond ((and (eq terminal :wxt) size)
	 `(quote (:wxt :size ,size :enhanced :font "Times-New-Roman,10" :persist)))
	((eq terminal :wxt)
	 `(quote (:wxt :enhanced :font "Times-New-Roman,10" :persist)))
	((and (eq terminal :png) size)
	 `(quote (:pngcairo :size ,size :enhanced :font "Times-New-Roman,10")))
	((eq terminal :png)
	 `(quote (:pngcairo :enhanced :font "Times-New-Roman,10")))
	((and (eq terminal :svg) size)
	 `(quote (:svg :size ,size :enhanced :font "Times-New-Roman,10")))
	((eq terminal :svg)
	 `(quote (:svg :enhanced :font "Times-New-Roman,10")))
	;; Font size is set to 17, as actual font size is ~60% of the set font size for
	;; the eps terminal, according to the docs for gnuplot 5.2.
	((and (eq terminal :eps) size)
	 `(quote (:postscript :eps :size ,size :enhanced :color :font "Times-Roman,17" :linewidth 2 :dl 4)))
	((eq terminal :eps)
	 `(quote (:postscript :eps :enhanced :color :font "Times-Roman,17" :linewidth 2 :dl 10)))
	((and (eq terminal :epslatex) size)
	 `(quote (:epslatex :size ,size :color :colortext)))
	((eq terminal :epslatex)
	 `(quote (:epslatex :enhanced :color :colortext)))
	(t
	 `(quote (:wxt :size |350,262| :enhanced :font "Times-New-Roman,10" :linewidth 2 :persist)))
	))

(defmacro gp-standard-setup (terminal size &rest args)
  "
  Works like gp-setup, except the first two arguments are a terminal
  and a terminal size, which are used to initialize the gnuplot
  terminal with commonly used settings. More information on these
  parameters is available in the gp-terminal-list function docstring.
  "
  (let* ((term-opts (gp-terminal-list terminal (eval size))))
    `(gp-setup :terminal ,term-opts ,@args)))

(defmacro gp-set-bw-lines ()
  "Sets default black and white line types for gnuplot."
  '(progn
    (gp :set :linetype 1 :lc :rgb "#000000" :lw 2 :dt 1)
    (gp :set :linetype 2 :lc :rgb "#000000" :lw 2 :dt 2)
    (gp :set :linetype 3 :lc :rgb "#000000" :lw 2 :dt 3)
    (gp :set :linetype 4 :lc :rgb "#000000" :lw 2 :dt 4)
    (gp :set :linetype 5 :lc :rgb "#000000" :lw 2 :dt 5)
    (gp :set :linetype 6 :lc :rgb "#000000" :lw 2 :dt 6)
    (gp :set :linetype 7 :lc :rgb "#000000" :lw 2 :dt 7)
    (gp :set :linetype 8 :lc :rgb "#000000" :lw 2 :dt 8)
    (gp :set :linetype :cycle 8)))

(defmacro gp-set-color-lines ()
  "
  Sets better default color line types for gnuplot.

  Colors come from colorbrewer2.
  "
  '(progn
    (gp :set :linetype 1 :lc :rgb "#e41a1c" :lw 2)
    (gp :set :linetype 2 :lc :rgb "#377eb8" :lw 2)
    (gp :set :linetype 3 :lc :rgb "#4daf4a" :lw 2)
    (gp :set :linetype 4 :lc :rgb "#984ea3" :lw 2)
    (gp :set :linetype 5 :lc :rgb "#ff7f00" :lw 2)
    (gp :set :linetype 6 :lc :rgb "#ffff33" :lw 2)
    (gp :set :linetype 7 :lc :rgb "#a65628" :lw 2)
    (gp :set :linetype 8 :lc :rgb "#f781bf" :lw 2)
    (gp :set :linetype :cycle 8)))

(defmacro plot-columns (columns &rest args)
  "
  Plots the data in COLUMNS using gnuplot.
  "
  `(plot (lambda () (iter (for i index-of-vector (first ,columns))
			  (after-each
			   (format t "~&~{~F ~}" (row ,columns i)))))
	 ,@args))

(defmacro plot-rows (rows &rest args)
  "
  Plots the data in ROWS using gnuplot.
  "
  `(plot (lambda () (iter (for pt in ,rows)
			  (after-each (format t "~&~{~F ~}" pt))))
	 ,@args))

(defmacro plot-complex-numbers (complex-vector &rest args)
  "
  Plots COMPLEX-VECTOR in the complex plane using gnuplot.
  "
  `(plot (lambda () (loop for el across ,complex-vector do
			  (format t "~&~F ~F" (realpart el) (imagpart el))))
	 ,@args))

(defmacro plot-circle (radius centerx centery npoints &rest args)
  "
  Plots a circle with radius R centered at (CENTERX, CENTERY)
  using NPOINTS.
  "
  `(plot-rows (circle-points ,radius ,centerx ,centery :npoints ,npoints)
	      ,@args :with '(:lines)))

(defmacro plot-smith-resistance-circle (r &key (npoints 256))
  "
  Plots a constant-resistance circle for resistance R on the smith chart.
  "
  `(plot-circle (/ (+ 1 ,r)) (/ ,r (+ 1 ,r)) 0 :npoints ,npoints))

(defmacro plot-smith-reactance-circle (x &key (npoints 256))
  `(plot-circle (/ ,x) 1 (/ ,x) :npoints ,npoints))

(defmacro plot-truncated-smith-reactance-circle (x &key (npoints 512))
  `(plot (plot-rows (circle-points-in-radius (abs (/ ,x)) 1 1 (/ ,x) :npoints ,npoints))
	 :with '(:lines) :lt -1))
