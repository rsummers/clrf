(in-package :clrf)

;;; Constants
;;; Useful physical constants

(defparameter *dB-per-neper* 8.686
  "Number of decibles per neper")

(defparameter *e0* 8.854e-12
  "Permittivity of free-space in F/m")

(defparameter *u0* (* 4 pi (expt 10 -7))
  "Permeability of free-space in H/m")

(defparameter *n0* 376.7
  "Impedance of free-space in ohms")

(defparameter *c* 2.998e8
  "Velocity of light in free-space in m/s")
