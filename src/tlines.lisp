(in-package :clrf)

;;; Microstrip Lines
;;; Definitions useful for working with microstrip transmission lines.

(defun microstrip-effective-dielectric-constant-0 (u er)
  "
  Calculates effective permeability of a microstrip line without
  accounting for copper thickness or the frequency dispersion effect.
  "
  (let ((a ($ 1 + (1 / 49) * (log (u ^ 4 + (u / 52) ^ 2) / (u ^ 4 + 0.432))
	      + (1 / 18.7) * (log 1 + (u / 18.1) ^ 3)))
	(b ($ 0.564 * ((er - 0.9) / (er + 3)) ^ 0.053)))
    ($ (er + 1) / 2 + ((er - 1) / 2) * (1 + 10 / u) ^ (-a * b))))

(defun microstrip-characteristic-impedance-1 (u &optional (er 1))
  "
  Calculates characteristic impedance of a microstrip line without
  accounting for copper thickness or the frequency dispersion effect.
  "
  (let ((fu ($ 6 + (2 * pi - 6) * (exp -1 * (30.666 / u) ^ 0.7528)))
	(n0 (/ *n0* (sqrt (microstrip-effective-dielectric-constant u er)))))
    ($ n0 / (2 * pi) * (log fu / u + (sqrt 1 + (2 / u) ^ 2)))))

(defun microstrip-homogenous-width-correction (u thickness)
  "
  Calculates the homogenous width correction factor described by
  Hammerstad and Jensen for microstrip lines.
  "
  ($ thickness / pi * (log 1 + 4 * (exp 1) / (thickness * (coth (sqrt 6.517 * u)) ^ 2))))

(defun microstrip-inhomogenous-width-correction (u thickness er)
  "
  Calculates the inhomogenous width correction factor described by
  Hammerstad and Jensen for microstrip lines.
  "
  (let ((delta-u1 (microstrip-homogenous-width-correction u thickness)))
    ($ 1/2 * (1 + 1 / (cosh (sqrt er - 1))) * delta-u1)))

(defun microstrip-homogenous-corrected-width (u thickness)
  "
  Calculates the corrected homogenous width described by Hammerstad
  and Jensen for microstrip lines.
  "
  (+ u (microstrip-homogenous-width-correction u thickness)))

(defun microstrip-inhomogenous-corrected-width (u thickness er)
  "
  Calculates the corrected inhomogenous width described by Hammerstad
  and Jensen for microstrip lines.
  "
  (+ u (microstrip-inhomogenous-width-correction u thickness er)))

(defun microstrip-effective-dielectric-constant
    (u er &key (thickness 0) (frequency 0) (sub-thickness 0))
  "
  Calculates the effective dielectric constant of a microstrip
  transmission line using the techniques developed by Hammerstad and
  Jensen and the improved formulas published by Kirschning and Jansen
  to account for the frequency dispersion effect.

  Required Parameters:
  - `u` is the trace width normalized to the substrate height (W/d).
  - `er`  is the relative permittivity of the substrate.

  Keyword Parameters:
  - `thickness` is the thickness of the copper normalized to the
    substrate height (T/d).
  - `frequency` is the frequency at which the effective dielectric
    constant is calculated for, in GHz. If this parameter is
    specified, `sub-thickness` must be specified as well.
  - `sub-thickness` is the thickness of the substrate in millimeters.
    This parameter has no effect unless frequency is specified as
    well.

  Returns:
  - The effective dielectric constant of the microstrip line.
  "
  (cond ((/= frequency 0)
	 (if (= sub-thickness 0)
	     (format t "microstrip-effective-dielectric-constant requires a sub-thickness to be provided for nonzero frequencies"))
	 (let* ((f frequency)
		(b sub-thickness)
		(p1 ($ 0.27488 + u * (0.6315 + 0.525 * (0.0157 * f * b + 1) ^ -20)
		       - 0.065683 * (exp -8.7513 * u)))
		(p2 ($ 0.33622 * (1 - (exp -0.03442 * er))))
		(p3 ($ 0.0363 * (exp -4.6 * u) * (1 - (exp -1 * (f * b / 38.7) ^ 4.97))))
		(p4 ($ 2.751 * (1 - (exp -1 * (er / 15.916) ^ 8)) + 1))
		(p ($ p1 * p2 * (f * b * (0.1844 + p3 * p4)) ^ 1.5763))
		(ee0 (microstrip-effective-dielectric-constant u er :thickness thickness)))
	   ($ er - (er - ee0) / (1 + p))))
	((/= thickness 0)
	 (let* ((u1 (microstrip-homogenous-corrected-width u thickness))
		(ur (microstrip-inhomogenous-corrected-width u thickness er))
		(zo1 (microstrip-characteristic-impedance-1 u1))
		(zor (microstrip-characteristic-impedance-1 ur))
		(ee0 (microstrip-effective-dielectric-constant-0 u er)))
	   ($ ee0 * (zo1 / zor) ^ 2)))
	(t (microstrip-effective-dielectric-constant-0 u er))))

(defun microstrip-characteristic-impedance
    (u er &key (thickness 0) (frequency 0) (sub-thickness 0))
  "
  Calculates the characteristic impedance of a microstrip
  transmission line using the techniques developed by Hammerstad and
  Jensen and the improved formulas published by Kirschning and Jansen
  to account for the frequency dispersion effect.

  Required Parameters:
  - `u` is the trace width normalized to the substrate height (W/d).
  - `er`  is the relative permittivity of the substrate.

  Keyword Parameters:
  - `thickness` is the thickness of the copper normalized to the
    substrate height (T/d).
  - `frequency` is the frequency at which the effective dielectric
    constant is calculated for, in GHz. If this parameter is
    specified, `sub-thickness` must be specified as well.
  - `sub-thickness` is the thickness of the substrate in millimeters.
    This parameter has no effect unless frequency is specified as
    well.

  Returns:
  - The characteristic impedance of the microstrip line in ohms.
  "
  (cond ((/= frequency 0)
	 (let ((zoo (microstrip-characteristic-impedance u er :thickness thickness))
	       (ee0 (microstrip-effective-dielectric-constant u er :thickness thickness))
	       (eef (microstrip-effective-dielectric-constant u er :thickness thickness :frequency frequency :sub-thickness sub-thickness)))
	   ($ zoo * (sqrt ee0 / eef) * (eef - 1) / (ee0 - 1))))
	((/= thickness 0)
	 (let* ((ur (microstrip-inhomogenous-corrected-width u thickness er))
		(zor (microstrip-characteristic-impedance-1 ur))
		(ee0 (microstrip-effective-dielectric-constant-0 u er)))
	   ($ zor / (sqrt ee0))))
	(t (microstrip-characteristic-impedance-1 u er))))

(defun microstrip-normalized-width
    (z0 er &key (thickness 0) (frequency 0) (sub-thickness 0) (max-iter 50) (precision 0.01d0))
  "
  Finds the normalized track width of a microstrip line with a given
  characteristic impedance, taking frequency and conductor thickness
  into account. This method finds the roots of
  microstrip-characteristic-impedance - z0 using the brent method as
  implemented in the GNU Scientific Library. It is assumed that the
  required normalized track width is between 0.0001 and 1000.

  Required Parameters:
  - `z0` is the characteristic impedance of the line, in ohms.
  - `er`  is the relative permittivity of the substrate.

  Keyword Parameters:
  - `thickness` is the thickness of the copper normalized to the
    substrate height (T/d).
  - `frequency` is the frequency at which the effective dielectric
    constant is calculated for, in GHz. If this parameter is
    specified, `sub-thickness` must be specified as well.
  - `sub-thickness` is the thickness of the substrate in millimeters.
    This parameter has no effect unless frequency is specified as
    well.
  - `max-iter` is the maximum number of iterations the brent solver
    will go through before returning.
  - `precision` is the desired precision of the solution.

  Returns:
  - The normalized track width of the microstrip line.
  "
  (let* ((impedance-function #'(lambda (u)
				 (- (microstrip-characteristic-impedance
				     u er
				     :thickness thickness
				     :frequency frequency
				     :sub-thickness sub-thickness) z0)))
	 (solver
	  (gsll:make-one-dimensional-root-solver-f
	   gsll:+brent-fsolver+ impedance-function 0.0001d0 1000.0d0)))
    (loop for iter from 0
	  for root = (gsll:solution solver)
	  for lower = (gsll:fsolver-lower solver)
	  for upper = (gsll:fsolver-upper solver)
	  do (gsll:iterate solver)
	  while (and (< iter max-iter)
		     (not (gsll:root-test-interval lower upper 0.0d0 precision)))
	  finally (return root))))

(defun make-microstrip-width-vector
    (impedances er &key (thickness 0) (frequency 0) (sub-thickness 0) (max-iter 50) (precision 0.01d0))
  "
  Returns a vector of track widths for the specified microstrip lines.

  Required Parameters:
  - `impedances` is a vector of characteristic impedances, in ohms.
  - `er`  is the relative permittivity of the substrate.

  Keyword Parameters:
  - `thickness` is the thickness of the copper normalized to the
    substrate height (T/d).
  - `frequency` is the frequency at which the effective dielectric
    constant is calculated for, in GHz. If this parameter is
    specified, `sub-thickness` must be specified as well.
  - `sub-thickness` is the thickness of the substrate in millimeters.
    This parameter has no effect unless frequency is specified as
    well.
  - `max-iter` is the maximum number of iterations the brent solver
    will go through before returning.
  - `precision` is the desired precision of the solution.

  Returns:
  - A vector of normalized widths for the supplied line parameters.
  "
  (map 'vector
       #'(lambda (z0) (microstrip-normalized-width z0 er :thickness thickness :frequency frequency :sub-thickness sub-thickness :max-iter max-iter :precision precision))
       impedances))


(defun microstrip-effective-dielectric-constant-vs-frequency
    (frequencies u er sub-thickness &key (thickness 0))
  (map 'vector
       #'(lambda (frequency) (microstrip-effective-dielectric-constant u er :thickness thickness :frequency frequency :sub-thickness sub-thickness))
       frequencies))

(defun microstrip-impedance-vs-frequency
    (frequencies u er sub-thickness &key (thickness 0))
  "
  Returns a vector of the characteristic impedance of the supplied microstrip
  line at given frequencies.

  Required Parameters:
  - `frequencies` is a vector of frequencies, in GHz.
  - `u` is the trace width normalized to the substrate height (W/d).
  - `er`  is the relative permittivity of the substrate.
  - `sub-thickness` is the thickness of the substrate in millimeters.

  Keyword Parameters:
  - `thickness` is the thickness of the copper normalized to the
    substrate height (T/d).

  Returns:
  - A vector of characteristic impedances in ohms.
  "
  (map 'vector
       #'(lambda (frequency) (microstrip-characteristic-impedance u er :thickness thickness :frequency frequency :sub-thickness sub-thickness))
       frequencies))

(defun microstrip-conductor-attenuation (rs z0 w)
  "
  Calculates the attenuation due to conductor loss in a
  microstrip line in Np/m given the surface resistivity,
  characteristic impedance, and width of the line.
  "
  (/ rs (* z0 w)))

(defun gamma<-z (zl &optional (z0 50))
  "
  Converts impedance to reflection coefficient.

  Required Parameters:
  - `zl` is the load impedance, in ohms.

  Optional Parameters:
  - `z0` is the line characteristic impedance, in ohms. If not
    specified, a characteristic impedance of 50 ohms is assumed.
  "
  (/ (- zl z0) (+ zl z0)))

(defun design-l-network (zl z0)
  "
  Calculates the parameters for an l-section matching network to
  match a load of impedance `zl` to a line of impedance `z0`.

  Required Parameters:
  - `zl` is the load impedance, in ohms.
  - `z0` is the line characteristic impedance, in ohms.

  Returns:
  A plist with the type, or configuration of the network, given by
  Figure 5.2 in Pozar's Microwave Engineering, and the two possible
  impedance combinations for the network.
  "
  (let ((rl (realpart zl))
	(xl (imagpart zl)))
    (if (> rl z0)
	(let* ((b1 ($ (xl + (sqrt rl / z0) * (sqrt rl ^ 2 + xl ^ 2 - z0 * rl)) / (rl ^ 2 + xl ^ 2)))
	       (b2 ($ (xl - (sqrt rl / z0) * (sqrt rl ^ 2 + xl ^ 2 - z0 * rl)) / (rl ^ 2 + xl ^ 2)))
	       (x1 ($ 1 / b1 + xl * z0 / rl - z0 / (b1 * rl)))
	       (x2 ($ 1 / b2 + xl * z0 / rl - z0 / (b2 * rl))))
	  `(:type :a :x1 ,x1 :b1 ,b1 :x2 ,x2 :b2 ,b2))
      (let* ((b1 ($ (sqrt rl * (z0 - rl)) - xl))
	     (b2 ($ - (sqrt rl * (z0 - rl)) - xl))
	     (x1 ($ (sqrt (z0 - rl) / rl) / z0))
	     (x2 ($ - (sqrt (z0 - rl) / rl) / z0)))
	`(:type :b :x1 ,x1 :b1 ,b1 :x2 ,x2 :b2 ,b2)))))

(defun design-single-stub-network (zl z0 type termination)
  "
  Calculates the parameters of a single stub tuning network
  that matches a load of ZL to a line of impedance Z0.

  Required parameters:
  - ZL is the load impedance, in ohms.
  - Z0 is the line characteristic impedance, in ohms.
  - TYPE is the type of tuning circuit to design, either :shunt or
    :series
  - TERMINATION is the termination of the matching stub, either
    :open or :short.

  Returns:
  A plist with the two principal solutions for d and l, the distance
  of the stub from the load, and the length of the stub, respectively.
  "
  (let ((rl (realpart zl))
	(xl (imagpart zl))
	(gl (realpart (/ zl)))
	(bl (imagpart (/ zl)))
	(y0 (/ z0))
	t1 t2 d1 d2 b1 b2 x1 x2 l1 l2)

    (when (eql type :shunt)
      (if (eql rl z0)
	(setf t1 ($ - xl / (2 * z0)))
	(progn
	  (setf t1 ($ (xl + (sqrt rl * ((z0 - rl) ^ 2 + xl ^ 2) / z0)) / (rl - z0)))
	  (setf t2 ($ (xl - (sqrt rl * ((z0 - rl) ^ 2 + xl ^ 2) / z0)) / (rl - z0))))))

    (when (eql type :series)
      (if (eql gl y0)
	  (setf t1 ($ - bl / (2 * y0)))
	(progn
	  (setf t1 ($ (bl + (sqrt gl * ((y0 - gl) ^ 2 + bl ^ 2) / y0)) / (gl - y0)))
	  (setf t2 ($ (bl - (sqrt gl * ((y0 - gl) ^ 2 + bl ^ 2) / y0)) / (gl - y0))))))

    (setf d1 (if (>= t1 0) ($ (atan t1) / (2 * pi)) ($ (pi + (atan t1)) / (2 * pi))))
    (if t2
	(setf d2 (if (>= t2 0) ($ (atan t2) / (2 * pi)) ($ (pi + (atan t2)) / (2 * pi)))))

    (when (eql type :shunt)
      (setf b1 ($ (rl ^ 2 * t1 - (z0 - xl * t1) * (xl + z0 * t1)) /
		  (z0 * (rl ^ 2 + (xl + z0 * t1) ^ 2))))
      (if t2 (setf b2 ($ (rl ^ 2 * t2 - (z0 - xl * t2) * (xl + z0 * t2)) /
			 (z0 * (rl ^ 2 + (xl + z0 * t2) ^ 2)))))
      (when (eql termination :open)
	(setf l1 ($ -1 / (2 * pi) * (atan b1 / y0)))
	(if t2 (setf l2 ($ -1 / (2 * pi) * (atan b2 / y0)))))
      (when (eql termination :short)
	(setf l1 ($ 1 / (2 * pi) * (atan y0 / b1)))
	(if t2 (setf l2 ($ 1 / (2 * pi) * (atan y0 / b2))))))

    (when (eql type :series)
      (setf x1 ($ (gl ^ 2 * t1 - (y0 - t1 * bl) * (bl + t1 * y0)) /
		  (y0 * (gl ^ 2 + (bl + y0 * t1) ^ 2))))
      (if t2 (setf x2 ($ (gl ^ 2 * t2 - (y0 - t2 * bl) * (bl + t2 * y0)) /
			 (y0 * (gl ^ 2 + (bl + y0 * t2) ^ 2)))))
      (when (eql termination :open)
	(setf l1 ($ 1 / (2 * pi) * (atan z0 / x1)))
	(if t2 (setf l2 ($ 1 / (2 * pi) * (atan z0 / x2)))))
      (when (eql termination :short)
	(setf l1 ($ -1 / (2 * pi) * (atan x1 / z0)))
	(if t2 (setf l2 ($ -1 / (2 * pi) * (atan x2 / z0))))))


    (if (minusp l1) (setf l1 (+ l1 1/2)))
    (if (minusp l2) (setf l2 (+ l2 1/2)))
    `(:d1 ,d1 :l1 ,l1 :d2 ,d2 :l2 ,l2)))
