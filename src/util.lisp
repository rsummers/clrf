(in-package :clrf)

(defun range (first &optional (second 1 second-supplied-p) (third nil third-supplied-p))
  "
  Create a vector over the range specified.

  If only one argument is supplied, returns a vector from 0 up to but
  not including the argument, with a step size of 1.

  If two arguments are supplied, returns a vector from the first
  argument up to but not including the second argument, with a step
  size of 1.

  If all three arguments are supplied, returns a vector from the first
  argument up to but not including the third, with a step size given
  by the second argument.

  This behavior is analogous to python's range().
  "
  (cond (third-supplied-p
	 (coerce (iota (round (/ (- third first) second)) :start first :step second) 'vector))
	(second-supplied-p
	 (coerce (iota (- second first) :start first) 'vector))
	(t
	 (coerce (iota first :start 0) 'vector))))

(defun linspace (start stop &key (num 50) (endpoint t) (retstep nil))
  "
  Returns a vector of linearly spaced numbers.

  Behavior is similar to numpy's linspace.

  The vector will start at `start` and end at `stop`, both inclusive
  unless `endpoint` is set to nil. The vector will consist of `num`
  values. If `retstep` is non-nil, linspace will return the step size
  as a second value.
  "
  (let* ((div (if endpoint (- num 1) num))
	 (delta (- stop start))
	 (step-size (/ delta div)))
    (if retstep
	(values (coerce (iota num :start start :step step-size) 'vector) step-size)
      (coerce (iota num :start start :step step-size) 'vector))))

;; Utilities for dealing with complex numbers

(defconstant j #c(0 1)
  "The imaginary number")

(defun arg (number)
  "Returns the argument of `number`."
  (atan (imagpart number) (realpart number)))

(defun polar (r theta)
  "Returns the complex number represented in polar form by the arguments provided."
  (* r (exp (* j theta))))

(defun rect (re im)
  "Returns the complex number represented in polar form by the arguments provided."
  (+ re (* j im)))

(defun absarg (number)
  "
  Returns the magnitude and phase of `number`.
  "
  (values (abs number) (arg number)))

(defun realimag (number)
  "
  Returns the real and imaginary parts of `number`.
  "
  (values (realpart number) (imagpart number)))

;; Utilities for dealing with vectors and csv files.

(defun flip-major (iterables)
  "
  Takes a list of ITERABLES and returns a list with a flipped major axis.
  That is, if ITERABLES is a list of rows, a list of columns is returned,
  and vice-versa.
  "
  (loop for i to (- (length (first iterables)) 1)
	collect (loop for iterable in iterables collect (elt iterable i))))

(defun flip-major-coerce-vectors (iterables)
  "
  Same behavior as FLIP-MAJOR, but coerces the columns/rows to vectors.
  "
  (loop for iter in (flip-major iterables) collect (coerce iter 'vector)))

(defun row (columns i)
  "
  Returns the Ith row in a list of columns.
  "
  (loop for col in columns collect (elt col i)))

(defun write-csv-columns (columns &key header filename)
  "
  Creates a CSV with a list of COLUMNS at FILENAME.
  Optionally, a HEADER row can be written as well.
  "
  (with-open-file
   (stream filename :direction :output :if-exists :supersede)
   (with-standard-io-syntax
    (cl-csv:write-csv (if header
			  (cons header (flip-major columns))
			(flip-major columns))
		      :stream stream))))

(defun read-csv-columns (filename &key header)
  "
  Returns a list of column vectors from the CSV located at FILENAME.
  If HEADER is non-nil, the first row will be excluded.
  "
  (let ((content (with-open-file
		  (stream filename)
		  (with-standard-io-syntax
		   (cl-csv:read-csv stream :data-map-fn
				    #'(lambda (data) (with-input-from-string (in data)
									     (read in))))))))
    (if header
	(flip-major-coerce-vectors (cdr content))
      (flip-major-coerce-vectors content))))

;; Define missing hyperbolic trig functions

(defun radius (x y)
  (sqrt (+ (expt x 2) (expt y 2))))

(defun coth (x)
  "Returns the hyperbolic cotangent of a number."
  (/ (tanh x)))

;; Stuff dealing with circles and arcs.

(defun arc-from-angles (radius centerx centery theta1 theta2 &key (npoints 256))
  "
  Returns a list of points on the arc described by the parameters.
  "
  (iter (for s in-vector (linspace theta1 theta2 :num npoints))
	(collect (list (+ centerx (* radius (cos s)))
		       (+ centery (* radius (sin s)))))))

(defun arc-from-endpoints (radius centerx centery point1 point2 &key (npoints 256))
  (let* ((x1 (first point1))
	 (y1 (second point1))
	 (x2 (first point2))
	 (y2 (second point2))
	 (s1 (atan (- y1 centery) (- x1 centerx)))
	 (s2 (atan (- y2 centery) (- x2 centerx))))
    (arc-points radius centerx centry s1 s2 :npoints npoints)))

(defun circle-points (radius centerx centery &key (npoints 256))
  "
  Returns a list of points on the circle described by the parameters.
  "
  (arc-from-angles radius centerx centery 0 (* 2 pi) :npoints npoints))

