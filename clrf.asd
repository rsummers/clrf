(defsystem "clrf"
  :version "0.0.1"
  :author "Randall Summers <rsummers@ou.edu>"
  :license "MIT"
  :depends-on ("gsll" "infix-math" "eazy-gnuplot" "cl-csv" "iterate")
  :components ((:module "src"
		:serial t
                :components
                ((:file "package")
		 (:file "constants")
		 (:file "util")
		 (:file "plotting")
		 (:file "tlines"))))
  :description "Tools for rf development using common lisp."
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.md")))
;;  :in-order-to ((test-op (test-op "clrf-test"))))
